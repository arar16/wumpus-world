import random


class WumpusGame(object):

    def __init__(self, edges=None):

        # Create arbitrary caves from a list of edges (see the end of the script for example).
        if edges is None:
            edges = []
        if edges:
            cave = {}
            n = max([edges[i][0] for i in range(len(edges))])
            for i in range(n):
                exits = [edge[1] for edge in edges if edge[0] == i]
                cave[i] = exits

        # If no edges are specified, play in the standard cave: a dodecahedron.
        else:
            cave = {
                1: [2, 5],
                2: [1, 3, 6],
                3: [2, 7, 4],
                4: [3, 8],
                5: [1, 6, 9],
                6: [2, 5, 7, 10],
                7: [3, 6, 8, 11],
                8: [4, 7, 12],
                9: [5, 10, 13],
                10: [6, 9, 11, 14],
                11: [7, 10, 12, 15],
                12: [8, 11, 16],
                13: [9, 14],
                14: [10, 13, 15],
                15: [11, 14, 16],
                16: [12, 15]
            }

        #Variables for the System

        self.cave = cave

        self.pits = []

        self.gold = []

        self.wumpus = []

        self.breezePos = []

        self.stenchPos = []

        self.glitterPos = []

        self.player_pos = 1

        #The road Agent has to go
        self.road = [1]

        #Places where the Agent was
        self.visited = []

        #Probably Pit and Wumpus Places
        self.pitFlag = []

        self.wumpusFlag = []

        #Detected Pits and Wumpus
        self.realPits = []

        self.realWumpus = []

    """
    HELPER: These methods wrap processes that are useful or called often.
    """

    def checkBreeze(self):
        if self.player_pos in self.breezePos:
            return True
        else:
            return False

    def checkStench(self):
        if self.player_pos in self.stenchPos:
            return True
        else:
            return False

    def checkGlitter(self):
        if self.player_pos in self.glitterPos:
            return True
        else:
            return False

    def checkThreat(self):
        if self.player_pos in self.pits or self.player_pos in self.wumpus:
            return True
        else:
            return False

    def makeMove(self):
        for i in self.road:
            print("i: ", i)
            self.player_pos = i
            self.visited.append(i)
            self.printBoard()

            #check if you`re in a threat
            if self.checkThreat():
                print("you Lost!")
                exit(0)

            #look at gold nearby
            if self.checkGlitter():
                for p in self.cave.get(i):
                    if p not in self.road and p not in self.pitFlag and p not in self.wumpusFlag:
                        #print("road added: ", p)
                        self.road.append(p)

            #check if you won
            if i in self.gold:
                print("You won!")
                exit(0)

            #Remove pit- and wumpus flags at that Places where we`ve been
            for a in self.visited:
                if a in self.pitFlag:
                    self.pitFlag.remove(a)
                if a in self.wumpusFlag:
                    self.wumpusFlag.remove(a)

            #if Place is save add neighbours to Road
            if not self.checkBreeze() and not self.checkStench():
                for x in self.cave.get(i):
                    if x not in self.road:
                        self.road.append(x)

            #if breeze and stench at the same time --> special operations for AI
            if self.checkStench() and self.checkBreeze():
                for x in self.cave.get(i):
                    if x not in self.wumpusFlag and x not in self.pitFlag and x not in self.road:
                        #print("road3 added: ", x)
                        self.road.append(x)
                continue

            #if breeze mark pit flags and save roads
            if self.checkBreeze():
                for x in self.cave.get(i):
                    if (x in self.pitFlag and x not in self.realPits) or x in self.realWumpus:
                        #print("realPits added: ", x)
                        self.realPits.append(x)
                    elif x not in self.pitFlag and x not in self.road:
                        self.pitFlag.append(x)
                        #print("Pitflag added: ", x)
                    if x in self.wumpusFlag and x not in self.road:
                        self.road.append(x)
                        #print("road1 added: ", x)

            #if stench mark wumpus flags and save roads
            elif self.checkStench():
                for x in self.cave.get(i):
                    if x in self.wumpusFlag and x not in self.realWumpus:
                        self.realWumpus.append(x)
                        #print("realWump added: ", x)
                    elif x not in self.wumpusFlag and x not in self.road:
                        self.wumpusFlag.append(x)
                        #print("WumpFlag added: ", x)
                    if x in self.pitFlag and x not in self.road:
                        self.road.append(x)
                        #print("road2 added: ", x)

            #remove fields from road if there are pits or wumpus in it
            for r in self.road:
                if r in self.realPits or r in self.realWumpus:
                    #print("removed: ", r)
                    self.road.remove(r)

            # print("Visited ", self.visited)
            # print("Road: ", self.road)
            # print("pitFlags: ", self.pitFlag)
            # print("wumpusFlags: ", self.wumpusFlag)
            # print("realPits: ", self.realPits)
            # print("realWumpus: ", self.realWumpus)

    def get_safe_rooms(self):
        #Returns a list containing all numbers of rooms that do not contain any threats
        return list(set(self.cave.keys()).difference(self.wumpus + self.pits + [1] + self.gold))

    """ 
        Drop gold and threats into random rooms in the cave.
    """

    def set_wumpus(self):

        pos = random.choice(self.get_safe_rooms())
        self.wumpus.append(pos)
        for i in self.cave.get(self.wumpus[0]):
            if i not in self.stenchPos:
                self.stenchPos.append(i)

    def set_gold(self):
        pos = random.choice(self.get_safe_rooms())
        self.gold.append(pos)
        for i in self.cave.get(self.gold[0]):
            if i not in self.glitterPos:
                self.glitterPos.append(i)

    def set_pits(self):
        for i in range(3):
            pos = random.choice(self.get_safe_rooms())
            self.pits.append(pos)

        for i in self.pits:
            for x in self.cave.get(i):
                if x not in self.breezePos:
                    self.breezePos.append(x)

    """
        prints the Board
    """

    def printBoard(self):
        print("Board:")
        x = 1
        for i in range(4):
            if x in self.wumpus:
                print("W", end="    |   ")
            elif x in self.pits:
                print("P", end="    |   ")
            elif x in self.gold:
                print("G", end="    |   ")
            elif x == self.player_pos:
                print("A", end="    |   ")
            else:
                print(".", end="    |   ")
            x += 1
        print()
        for i in range(4):
            if x in self.wumpus:
                print("W", end="    |   ")
            elif x in self.pits:
                print("P", end="    |   ")
            elif x in self.gold:
                print("G", end="    |   ")
            elif x == self.player_pos:
                print("A", end="    |   ")
            else:
                print(".", end="    |   ")
            x += 1
        print()
        for i in range(4):
            if x in self.wumpus:
                print("W", end="    |   ")
            elif x in self.pits:
                print("P", end="    |   ")
            elif x in self.gold:
                print("G", end="    |   ")
            elif x == self.player_pos:
                print("A", end="    |   ")
            else:
                print(".", end="    |   ")
            x += 1
        print()
        for i in range(4):
            if x in self.wumpus:
                print("W", end="    |   ")
            elif x in self.pits:
                print("P", end="    |   ")
            elif x in self.gold:
                print("G", end="    |   ")
            elif x == self.player_pos:
                print("A", end="    |   ")
            else:
                print(".", end="    |   ")
            x += 1
        print()
        print()

    """
        Game starting Loop
    """

    def gameloop(self):

        print("HUNT THE WUMPUS")
        print("===============")
        print()
        self.set_wumpus()
        self.set_gold()
        self.set_pits()
        self.printBoard()
        # print("Wumpus: ", self.wumpus)
        # print("Pits: ", self.pits)
        # print("gold: ", self.gold)
        self.makeMove()


if __name__ == '__main__':
    WG = WumpusGame()
    WG.gameloop()
